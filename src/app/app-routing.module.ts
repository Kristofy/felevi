import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ProjectsComponent } from './projects/projects.component';
import { FundamentalsComponent } from './fundamentals/fundamentals.component';
import { LanguagesComponent } from './languages/languages.component';
import { ZtypeComponent } from './projects/ztype/ztype.component';
import { SnailComponent } from './projects/snail/snail.component';
import { SnowComponent } from './projects/snow/snow.component';
import { MazeGeneratorComponent } from './projects/maze-generator/maze-generator.component';
import { BoubleSortComponent } from './projects/bouble-sort/bouble-sort.component';
import { SimpleSortComponent } from './projects/simple-sort/simple-sort.component';
import { SelectionSortComponent } from './projects/selection-sort/selection-sort.component';
import { InsertionSortComponent } from './projects/insertion-sort/insertion-sort.component';
import { Dusza2018Component } from './projects/dusza2018/dusza2018.component';
import { EquationComponent } from './projects/equation/equation.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  {path:'home', component:HomeComponent},
  {path:'', redirectTo:'home', pathMatch:'full'},
  {path:'about', component:AboutComponent},
  {path:'projects/equation', component:EquationComponent},
  {path:'projects/maze-generator', component:MazeGeneratorComponent},
  {path:'projects/bouble-sort', component:BoubleSortComponent},
  {path:'projects/dusza-2018', component:Dusza2018Component},
  {path:'projects/simple-sort', component:SimpleSortComponent},
  {path:'projects/selection-sort', component:SelectionSortComponent},
  {path:'projects/insertion-sort', component:InsertionSortComponent},
  {path:'projects/snow', component:SnowComponent},
  {path:'projects/ztype', component:ZtypeComponent},
  {path:'projects/snail', component:SnailComponent},
  {path:'projects', component:ProjectsComponent},
  {path:'fundamentals', component:FundamentalsComponent},
  {path:'languages', component:LanguagesComponent},
 // page not found
  {path:'**', component:PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
