import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import {DialogModule} from 'primeng/dialog';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ProjectsComponent } from './projects/projects.component';
import { FundamentalsComponent } from './fundamentals/fundamentals.component';
import { LanguagesComponent } from './languages/languages.component';
import { ZtypeComponent } from './projects/ztype/ztype.component';
import { SnailComponent } from './projects/snail/snail.component';
import { SnowComponent } from './projects/snow/snow.component';
import { MazeGeneratorComponent } from './projects/maze-generator/maze-generator.component';
import { BoubleSortComponent } from './projects/bouble-sort/bouble-sort.component';
import { SimpleSortComponent } from './projects/simple-sort/simple-sort.component';
import { SelectionSortComponent } from './projects/selection-sort/selection-sort.component';
import { InsertionSortComponent } from './projects/insertion-sort/insertion-sort.component';
import { Dusza2018Component } from './projects/dusza2018/dusza2018.component';
import { EquationComponent } from './projects/equation/equation.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ProjectsComponent,
    FundamentalsComponent,
    LanguagesComponent,
    ZtypeComponent,
    SnailComponent,
    SnowComponent,
    MazeGeneratorComponent,
    BoubleSortComponent,
    SimpleSortComponent,
    SelectionSortComponent,
    InsertionSortComponent,
    Dusza2018Component,
    EquationComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,

    BrowserAnimationsModule,
    FontAwesomeModule,
    DialogModule,
    FormsModule,

    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
