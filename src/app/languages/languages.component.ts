import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.css']
})
export class LanguagesComponent implements OnInit {

  languages:Language[]=[];

  constructor() { }

  ngOnInit() {
  this.languages = [
  {
    name:'C++',
    pros:'Rendkívül gyors, sok lehetőséget ad a használt memória kezelésére.',
    cons:'Nehezen hozható létre grafikus felület.',
    more:'https://hu.wikipedia.org/wiki/C%2B%2B',
    src: '../../assets/img/cplusplusLogo.png'
  },
  {
    name:'C#',
    other:'(.NET keretrendszer)',
    pros:'A Windows fejleszti, ezért Windows operációs rendszeren teljes mértékben támogatott.',
    cons:'Nem windowsos OS eken nem, vagy csak kis mértékben támogatott (pl.: Android, IOS, Linux)',
    more: 'https://hu.wikipedia.org/wiki/C_Sharp',
    src: '../../assets/img/csharpLogo.png'
  },
  {
    name:'Javascript',
    pros:'Egyszerű mégis képes bonyulult feladatok végrehejtására, alapértelmazetten asynchronous.',
    cons:'Asynchronous, nem irányítható olyan mértékben mint pléldául a C++ (pl.: ha egy obijektumot egyenlővé teszünk egy másikkal, alapértelmezetten referencia szerint (pointer ként) teszi)',
    more:'https://hu.wikipedia.org/wiki/JavaScript',
    src: '../../assets/img/jsLogo.png'
  },
  {
    name:'HTML',
    pros:'Egyszerú nyelv, könnyen áttlátható kód.',
    cons:'Egymagában nem képes dinamikus tartalmat megjeleníteni',
    more:'https://hu.wikipedia.org/wiki/HTML',
    src: '../../assets/img/htmlLogo.png'
  },
  {
    name:'CSS',
    other:'(alternatívák: Bootstrap, SASS)',
    pros:'Kissebb projectek esetében átlátható kódot, egyszerű használatot.',
    cons:'Nagyobb projectek esetében nehezen olvasható kód.',
    more:'https://hu.wikipedia.org/wiki/CSS',
    src: '../../assets/img/cssLogo.png'
  },
  {
    name:'Typescript',
    pros:'Obiektum orientált, a javascript összes előnyével rendelkezik.',
    cons:'Mielőtt használhatnánk átt kell konvertálni javascript formátumba.',
    more:'https://en.wikipedia.org/wiki/TypeScript',
    src: '../../assets/img/tsLogo.png'
  }
  ];
  }

}

interface Language {
  name:string,
  other?:string,
  pros:string,
  cons:string,
  more:string,
  src:string
}