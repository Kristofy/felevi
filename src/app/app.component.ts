import { Component, OnInit } from '@angular/core';


import { faSearch, faHome, faAddressBook, faProjectDiagram, faFileCode, faGlobe } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  faSearch=faSearch;
  faHome=faHome;
  faAddressBook=faAddressBook;
  faProjectDiagram=faProjectDiagram;
  faFileCode=faFileCode;
  faGlobe=faGlobe;
  ngOnInit() {
    
  }
}
