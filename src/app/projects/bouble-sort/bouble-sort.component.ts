import { Component, OnInit } from '@angular/core';
import * as p5 from '../../../../node_modules/p5';

@Component({
  selector: 'app-bouble-sort',
  templateUrl: './bouble-sort.component.html',
  styleUrls: ['./bouble-sort.component.css']
})
export class BoubleSortComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    var boublesort = new p5(p=>{

    var arr = [];
    var m;
    var x;
    p.setup = function (){
      var C = p.createCanvas(800,400);
      C.parent('#parent');
      p.colorMode(p.HSB);
      for(var i = 0; i < 800; i ++){
        var a = p.random(p.height)
      arr[i]=new szam(a,a/p.height*360);
      }
      m=(p.width)/arr.length;
      p.noStroke();
      x = arr.length-1;
      setInterval(show,1);
    }

    p.draw = function() {
      for(var k = 0; k < x; k++){
      
      if(arr[k].ertek > arr[k+1].ertek){
        
        var tmp = arr[k];
        arr[k] = arr[k+1];
        arr[k+1] = tmp;
      }
      }
      x--;
      if(x < 1){
      p.noLoop();
        }
      
    }

    function show(){
      p.background(0);
      for(var i = 0; i < arr.length; i++){
        p.fill(arr[i].szin,100,90);
        p.rect(i*m,p.height-arr[i].ertek,1,arr[i].ertek);
      }
    }
    

    class szam{
      ertek;
      szin;
      constructor(ertek,szin){
      this.ertek=ertek;
      this.szin=szin;
      }
      
    }


    });

  }

}
