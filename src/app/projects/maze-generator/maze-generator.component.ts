import { Component, OnInit } from "@angular/core";
import * as p5 from "../../../../node_modules/p5";

@Component({
  selector: "app-maze-generator",
  templateUrl: "./maze-generator.component.html",
  styleUrls: ["./maze-generator.component.css"]
})
export class MazeGeneratorComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    var mazegenerator = new p5(p => {
      var rows; //sorok
      var cols; //oszlopok
      var m = 20; //szélesség
      var maze; //a labirintus alapja
      var current; //a jelenlegi cella
      var lista = []; //az eddig megtett lépések

      p.setup = function() {
        var C = p.createCanvas(400,400);
        C.parent('#parent');
        rows = p.floor(p.width / m);
        cols = p.floor(p.height / m);
        //1.Rész
        maze = make2DArray(rows, cols);
        current = maze[0][0];
        current.visited = true;
        p.colorMode(p.HSB);
      }

      function make2DArray(rows, cols) {
        var array = [];
        for (var i = 0; i < rows; i++) {
          array[i] = [];
          for (var j = 0; j < cols; j++) {
            array[i][j] = new Cella(i, j);
          }
        }
        return array;
      }

      p.draw = function() {
        p.background(51);
        for (var i = 0; i < rows; i++) {
          for (var j = 0; j < cols; j++) {
            maze[i][j].show();
          }
        }
        //2.1 Rész
        var kovetkezo = current.valassz();
        if (kovetkezo) {
          // ha a következőnek van értéke
          kovetkezo.visited = true;
          //2.2Rész
          lista.push(current);
          //2.3 Rész
          falak(current, kovetkezo);
          //2.4 Rész
          current = kovetkezo;
        } else {
          //3 Rész
          if (lista.length == 0) {
            p.noLoop();
          } else {
            current = lista.pop();
          }
        }
      }

      function falak(C, K) {
        if (C.i > K.i) {
          //current felső és a kovetkezo also részét kell elávolíttani
          C.fent = false;
          K.lent = false;
        }
        if (C.i < K.i) {
          //current also és a kovetkezo felső részét kell elávolíttani
          C.lent = false;
          K.fent = false;
        }
        if (C.j > K.j) {
          //current bal és a kovetkezo jobb részét kell elávolíttani
          C.bal = false;
          K.jobb = false;
        }
        if (C.j < K.j) {
          //current jobb és a kovetkezo bal részét kell elávolíttani
          C.jobb = false;
          K.bal = false;
        }
      }

      class Cella {
        i;j;y;x;visited;fent;jobb;lent;bal;
        constructor(y, x) {
          this.i = y;
          this.j = x;
          this.y = y * m;
          this.x = x * m;
          this.visited = false;
          this.fent = true;
          this.jobb = true;
          this.lent = true;
          this.bal = true;
        }

        valassz() {
          var szomszed = [];
          if (this.i - 1 >= 0) {
            var fent = maze[this.i - 1][this.j];
            if (!fent.visited) {
              szomszed.push(fent);
            }
          }
          if (this.j + 1 < cols) {
            var jobb = maze[this.i][this.j + 1];
            if (!jobb.visited) {
              szomszed.push(jobb);
            }
          }
          if (this.i + 1 < rows) {
            var lent = maze[this.i + 1][this.j];
            if (!lent.visited) {
              szomszed.push(lent);
            }
          }
          if (this.j - 1 >= 0) {
            var bal = maze[this.i][this.j - 1];
            if (!bal.visited) {
              szomszed.push(bal);
            }
          }
          if (szomszed.length > 0) {
            return p.random(szomszed);
          } else {
            return undefined;
          }
        }

        show() {
          if (this.visited) {
            p.noStroke();
            p.fill((this.i / rows) * 180 + (this.j / cols) * 180, 100, 90);
            p.rect(this.x, this.y, m, m);
          }
          if (this === current) {
            p.noStroke();
            p.fill(0, 100, 90);
            p.rect(this.x, this.y, m, m);
          }
          p.stroke(255);
          p.strokeWeight(1);
          if (this.fent) {
            p.line(this.x, this.y, this.x + m, this.y);
          }
          if (this.jobb) {
            p.line(this.x + m, this.y, this.x + m, this.y + m);
          }
          if (this.lent) {
            p.line(this.x + m, this.y + m, this.x, this.y + m);
          }
          if (this.bal) {
            p.line(this.x, this.y + m, this.x, this.y);
          }
        }
      }

      //Wikipédia: https://en.wikipedia.org/wiki/Maze_generation_algorithm
    });
  }
}
