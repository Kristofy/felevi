import { Component, OnInit } from '@angular/core';
import * as p5 from '../../../../node_modules/p5'
@Component({
  selector: 'app-snow',
  templateUrl: './snow.component.html',
  styleUrls: ['./snow.component.css']
})
export class SnowComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    var snow = new p5(p=>{
      let hoeses;
      let db=1;
      let mehet=true;
      p.setup = function() {
        let canvas = p.createCanvas(400,400);
        canvas.parent("parent");
        canvas.mousePressed(valt);
        p.frameRate(30);
        hoeses=[];
      }
      
      function valt(){
      mehet= !mehet;
      if(mehet){p.loop();}else{p.noLoop();}
      }
      
      p.draw = function() {
        p.background(0);
        for(let i=0;i<db;i++){
            hoeses.push(new ho());
        }
        for(let h of hoeses){
          h.show();
          h.gravity();
        }
        for(let i= hoeses.length-1;i>0;i--){
          if(hoeses[i].y>p.height+hoeses[i].r){
            hoeses.splice(i,1);
            
          }
        }
      }
      
      class ho{
        x;
        y;
        v;
        r;
        constructor(){
          this.x=p.random(p.width);
          this.y=-50;
          this.v=p.random(10,20);
          this.r=p.random(2,5);
        }
        gravity(){
          this.y+=this.v;
        }
        show(){
          p.ellipse(this.x,this.y,this.r*2)
        }
      
      }
    });

  }

}
