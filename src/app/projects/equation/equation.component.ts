import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-equation',
  templateUrl: './equation.component.html',
  styleUrls: ['./equation.component.css']
})
export class EquationComponent implements OnInit {
  a:number;
  b:number;
  c:number;
  x1:number;
  x2:number;
  D:number;
  message:string;
  constructor() { }

  ngOnInit() {  }

  solve():void{
    let mD = (this.b*this.b)-(4*this.a*this.c);

    if(mD<0){
        this.D=mD;
        this.x1=null;
        this.x2=null;
        this.message='negatív szám a gyökalatt';
        return;
    }
    let m1 = ((-1*this.b)+Math.sqrt(mD))/(2*this.a);
    let m2 = ((-1*this.b)-Math.sqrt(mD))/(2*this.a);
    this.D=mD;
    this.x1=m1;
    this.x2=m2;
    this.message='success';
    
  }

}
