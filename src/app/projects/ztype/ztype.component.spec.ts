import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZtypeComponent } from './ztype.component';

describe('ZtypeComponent', () => {
  let component: ZtypeComponent;
  let fixture: ComponentFixture<ZtypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZtypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZtypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
