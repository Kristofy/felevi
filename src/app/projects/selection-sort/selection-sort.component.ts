import { Component, OnInit } from '@angular/core';
import * as p5 from '../../../../node_modules/p5'

@Component({
  selector: 'app-selection-sort',
  templateUrl: './selection-sort.component.html',
  styleUrls: ['./selection-sort.component.css']
})
export class SelectionSortComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    var insertionsort = new p5(p=>{

      var arr = [];
      var m;
      var x = 0;
      p.setup = function() {
        var C = p.createCanvas(800,400);
        C.parent('#parent');
        p.colorMode(p.HSB);
        for (var i = 0; i < 800; i ++) {
          var a = p.random(p.height)
            arr[i]=new szam(a, a/p.height*360);
        }
        m=(p.width)/arr.length;
        p.noStroke();
        setInterval(show, 1);
      }
      
      p.draw = function() {
        var legk = x;
        for (var j = x+1; j < arr.length; j++) {
          if (arr[j].ertek < arr[legk].ertek) {
            legk = j;
          }
        }
        var temp = arr[legk];
        arr[legk] = arr[x];
        arr[x] = temp;
        x++;
          if (x >= arr.length-1) {
          p.noLoop();
        }
      }
      
      function show() {
        p.background(0);
        for (var i = 0; i < arr.length; i++) {
          p.fill(arr[i].szin, 100, 90);
          p.rect(i*m, p.height-arr[i].ertek, 1, arr[i].ertek);
        }
      }
      
      
      class szam {
        ertek;
        szin;
        constructor(ertek, szin) {
          this.ertek=ertek;
          this.szin=szin;
        }
      }
      

    });

  }

}
