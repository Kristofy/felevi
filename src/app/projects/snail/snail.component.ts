import { Component, OnInit } from '@angular/core';
import * as p5 from '../../../../node_modules/p5'
import { compileNgModuleFactory__POST_NGCC__ } from '@angular/core/src/application_ref';
@Component({
  selector: 'app-snail',
  templateUrl: './snail.component.html',
  styleUrls: ['./snail.component.css']
})
export class SnailComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    var snail = new p5(p=>{
    let rows, cols, m = 20;
    let arr;
    var left;
    p.setup = function() {
        var C = p.createCanvas(400, 400);
        C.parent('#parent')
        p.background(51);
        rows = p.height / m;
        cols = p.width / m;
        left=rows*cols;
        p.noFill();
        p.stroke(255);
        arr = [];
        for (var i = 0; i < rows; i++) {
            arr[i] = [];
            for (var j = 0; j < cols; j++) {
                arr[i][j] = false;
                p.rect(j * m, i * m, m, m);
            }
        }
    }
    var v = {
        x: 1,
        y: 0
    };
    var curr = {
        i: 0,
        j: 0,
        v
    };

   p.draw = function() {
        if (curr.i + curr.v.x< cols && curr.j + curr.v.y < rows && curr.i + curr.v.x>=0&&curr.j + curr.v.y>=0) {
            if (arr[curr.i+curr.v.x][curr.j+curr.v.y] === false) {
                arr[curr.i][curr.j] = true;
                p.fill(200, 100, 23);

            }else if (curr.v.x > 0) {
                curr.v.x = 0;
                curr.v.y = 1;
            }else if(curr.v.y>0){
                curr.v.y=0;
                curr.v.x=-1;
            }else if(curr.v.x<0){
                curr.v.x=0;
                curr.v.y=-1;
            }else if(curr.v.y<0){
                curr.v.y=0;
                curr.v.x=1;
            }



        } else {
            if (curr.v.x > 0) {
                curr.v.x = 0;
                curr.v.y = 1;
            }else if(curr.v.y>0){
                curr.v.y=0;
                curr.v.x=-1;
            }else if(curr.v.x<0){
                curr.v.x=0;
                curr.v.y=-1;
            }
        }
        p.rect(curr.i * m, curr.j * m, m, m);

        curr.i += curr.v.x;
        curr.j += curr.v.y;
        left--;
        if(left<=0){
          p.noLoop();
        }

        }
        
    });

  }

}
