import { Component, OnInit } from "@angular/core";
import * as p5 from "../../../../node_modules/p5";

@Component({
  selector: "app-simple-sort",
  templateUrl: "./simple-sort.component.html",
  styleUrls: ["./simple-sort.component.css"]
})
export class SimpleSortComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    var simplesort = new p5(p => {
      var arr = [];
      var m;
      var i = 0;
      p.setup=function() {
       
        var C = p.createCanvas(800,400);
        C.parent('#parent');
        p.colorMode(p.HSB);
        for (var i = 0; i < 800; i++) {
          var a = p.random(p.height);
          arr[i] = new szam(a, (a / p.height) * 360);
        }
        m = p.width / arr.length;
        p.noStroke();
        setInterval(show, 1);
      }

      p.draw = function() {
        for (var k = i + 1; k < arr.length; k++) {
          if (arr[i].ertek > arr[k].ertek) {
            var tmp = arr[i];
            arr[i] = arr[k];
            arr[k] = tmp;
          }
        }

        i++;
        if (i >= arr.length - 1) {
          p.noLoop();
        }
      }

      function show() {
        p.background(0);
        for (var i = 0; i < arr.length; i++) {
          p.fill(arr[i].szin, 100, 90);
          p.rect(i * m, p.height - arr[i].ertek, 1, arr[i].ertek);
        }
      }

      class szam {
        ertek;
        szin;
        constructor(ertek, szin) {
          this.ertek = ertek;
          this.szin = szin;
        }
      }
    });
  }
}
