import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleSortComponent } from './simple-sort.component';

describe('SimpleSortComponent', () => {
  let component: SimpleSortComponent;
  let fixture: ComponentFixture<SimpleSortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleSortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleSortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
