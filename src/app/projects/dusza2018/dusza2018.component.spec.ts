import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dusza2018Component } from './dusza2018.component';

describe('Dusza2018Component', () => {
  let component: Dusza2018Component;
  let fixture: ComponentFixture<Dusza2018Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dusza2018Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dusza2018Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
