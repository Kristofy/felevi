import { Component, OnInit } from '@angular/core';
import * as p5 from '../../../../node_modules/p5'

@Component({
  selector: 'app-dusza2018',
  templateUrl: './dusza2018.component.html',
  styleUrls: ['./dusza2018.component.css']
})
export class Dusza2018Component implements OnInit {
reset():void{};

  constructor() { }

  ngOnInit() {

    var dusza2018 = new p5(p=>{
      let col=0;
      let row=0;
      let m=50;
      let house;
      let path;
      let rooms;
      let first=true;
      
      
        this.reset = function(){
          row=parseInt((<HTMLInputElement>document.querySelector('#row')).value);
          col=parseInt((<HTMLInputElement>document.querySelector('#col')).value);
          path=(<HTMLInputElement>document.querySelector('#path')).value.toString().split('').filter((c)=> (c=='1'||c=='0')? true:false);
          rooms=0;
          let del = document.querySelector('h1') || null;
          (del) ? del.parentNode.removeChild(del):null;
          house=[];
          p.setup();
      
      }
      p.setup = function() {
          var C=p.createCanvas(col*m,row*m);
          C.parent('#canvas');
          if(first){first=false;return;}
          p.background(51);
          p.stroke(255);
          p.textSize(30);
          p.textAlign(p.CENTER, p.CENTER)
          console.log('cols and rows: '+ col + ' ' + row)
      // adatok beolvasása
          for(let i=0;i<row;i++){
              house[i]=[];
              for(let j=0;j<col;j++){
                  house[i][j]=new Bit(i*m,j*m,path[i*col+j]);
              }
          }
      // falak kezelése 
      
          search(0,0,1,'F')
      //szobák kezelése és számolása
          for(let i=1;i<row-1;i++){
              for(let j=0;j<col-1;j++){
                  if(house[i][j].type==0){
                      rooms++;
                      search(i,j,0,'Sz');
                  }
              }
          }
      // székek keresése
          for(let i=1;i<row-1;i++){
              for(let j=0;j<col-1;j++){
                  if(house[i][j].type==1&&house[i+1][j].type==='Sz'&&house[i-1][j].type==='Sz'&&house[i][j+1].type==='Sz'&&house[i][j-1].type==='Sz'){
                      house[i][j].type='S';
                  }
              }
          }
      // asztal keresés
          for(let i=1;i<row-1;i++){
              for(let j=1;j<col-1;j++){
                  if( house[i][j].type==1&&(
                      (house[i][j-1].type==1&&house[i-1][j-1].type==1&&house[i-1][j].type==1)||
                      (house[i-1][j].type==1&&house[i-1][j+1].type==1&&house[i][j-1].type==1)||
                      (house[i][j+1].type==1&&house[i+1][j+1].type==1&&house[i+1][j].type==1)||
                      (house[i+1][j].type==1&&house[i+1][j-1].type==1&&house[i][j-1].type==1))){
                      search(i,j,1,'A');
                  }
              }
          }
      // a maradék elvileg csak kanapé
          for(let i=1;i<row-1;i++){
              for(let j=0;j<col-1;j++){
                  if(house[i][j].type==1){
                      search(i,j,1,'K');
                  }
              }
          }
          
      // kirajzolás  
          for(let rows of house){
              for(let bit of rows){
                  bit.show();
              }
          }
      // kiirás
          var szobak=document.createElement('H4');
          szobak.textContent=`Szobák száma: ${rooms}`;
          document.body.append(szobak);
      }
      
      function search(i,j,searchType,nextType){
          if(i>=0&&i<row&&j>=0&&j<col){
              if(house[i][j].type==searchType){
                  house[i][j].type=nextType;
                  search(i+ 1,j, searchType, nextType);
                  search(i- 1,j, searchType, nextType);
                  search(i, j+1, searchType, nextType);
                  search(i, j-1, searchType, nextType);
              }
          }
      }
      
      //egy épületrész
      class Bit{
          x;
          y;
          type;
          constructor(y,x,type){
              this.x=x;
              this.y=y;
              this.type=type;
          }
          show(){
              if(this.type==='F'){
                  p.fill(200)
              } else if(this.type==='Sz'){
                  p.fill(0,200,0);
              }else if(this.type==='S'){
                  p.fill(0,100,100);
              }else if(this.type==='A'){
                  p.fill(139,69,19);
              }else{
                  p.fill(255,222,173);
              }
              p.rect(this.x,this.y,m,m);
              p.fill(0,0,120);
              p.text(this.type,this.x+m/2,this.y+m/2)
          }
      
      }

    });
  }

}
