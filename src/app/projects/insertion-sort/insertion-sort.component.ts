import { Component, OnInit } from '@angular/core';
import * as p5 from '../../../../node_modules/p5'

@Component({
  selector: 'app-insertion-sort',
  templateUrl: './insertion-sort.component.html',
  styleUrls: ['./insertion-sort.component.css']
})
export class InsertionSortComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    var insertionsort = new p5(p=>{

    var arr = [];
    var m;
    var x = 1;
    p.setup = function() {
      var C = p.createCanvas(800,400);
      C.parent('#parent');
      p.colorMode(p.HSB);
      for (var i = 0; i < 800; i ++) {
        var a = p.random(p.height)
          arr[i]=new szam(a, a/p.height*360);
      }
      m=(p.width)/arr.length;
      p.noStroke();
      setInterval(show, 1);
    }

    p.draw = function() {
      if (x>=arr.length) {
        p.noLoop();
      } else {
        var current = arr[x];
        var j = x - 1;
        while (j >= 0 && arr[j].ertek > current.ertek)
        {
          arr[j+1] = arr[j];
          j--;
        }
        arr[j+1] = current;
      }
      x++;
    }

    function show() {
      p.background(0);
      for (var i = 0; i < arr.length; i++) {
        p.fill(arr[i].szin, 100, 90);
        p.rect(i*m, p.height-arr[i].ertek, 1, arr[i].ertek);
      }
    }


    class szam {
      ertek;
      szin;
      constructor(ertek, szin) {
        this.ertek=ertek;
        this.szin=szin;
      }
    }

    });

  }

}
