import { Component, OnInit } from '@angular/core';
import { Options } from 'selenium-webdriver/ie';


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  projects:Projects[]=[];
  curr:Projects=<any>{};
  options:Option[]=[];

  constructor() { }

  sortOptions(value){
    value = this.options.filter(e=>value==e.name)[0].value;
    this.projects.sort((a,b)=>a[value].localeCompare(b[value]));
  }

  ngOnInit() {

  this.options=[
    {name:'cím szerint',value:'title'},
    {name:'típus szerint',value:'type'},
  ]; 




  this.projects=[{
      title: "Ztype",
      disc: "Egy egyszerű játék, a megjelenő szavakat kell időben legépelni",
      type: "p5js",
      link: "ztype",
      thumbnail: "../../assets/img/ztype.jpg"
  },
  {
      title: "Snail",
      disc: "Egy tömb bejárása csiga alakzatban",
      type: "p5js",
      link: "snail",
      thumbnail: "../../assets/img/snail.jpg"
  },
  {
      title: "Snow",
      disc: "Hóesés",
      type: "p5js",
      link: "snow",
      thumbnail: "../../assets/img/snow.jpg"
  },
  {
      title: "Simple sort",
      disc: "Egyszerű rendezés",
      type: "p5js",
      link: "simple-sort",
      thumbnail: "../../assets/img/simple-sort.jpg"
  },
  {
      title: "Bouble sort",
      disc: "Buborékos rendezés",
      type: "p5js",
      link: "bouble-sort",
      thumbnail: "../../assets/img/bouble-sort.jpg"
  },
  {
      title: "Selection sort",
      disc: "Kiválasztásos rendezés",
      type: "p5js",
      link: "selection-sort",
      thumbnail: "../../assets/img/selection-sort.jpg"
  },
  {
      title: "Insertion sort",
      disc: "Beillesztéses rendezés",
      type: "p5js",
      link: "insertion-sort",
      thumbnail: "../../assets/img/insertion-sort.jpg"
  },
  {
      title: "Maze generator",
      disc: "Labirintus generálás visszalépessel",
      type: "p5js",
      link: "maze-generator",
      thumbnail: "../../assets/img/maze-generator.jpg"
  },
  {
      title: "Lakás feltérképezés",
      disc: "Egy binális tervrajz alapján meghatározza a szobák számát és a benne található tárgyakat.",
      type: "p5js",
      link: "dusza-2018",
      thumbnail: "../../assets/img/dusza.jpg"
  },
  {
      title: "Megoldóképlet",
      disc: "Megold egy másodfokú egyenletet.",
      type: "Javascript && HTML",
      link: "equation",
      thumbnail: "../../assets/img/equation.jpg"
  },
  {
    title: "Console -os játék",
    disc: "A tavalyi félévi projektem",
    type: "C++",
    dwn: "../../assets/felevi_2017.zip",
    thumbnail: "../../assets/img/cplusplusLogo.png"
  },
  {
    title: "Console Warrior",
    disc: "Egy RPG játék konzolban.",
    type: "C++",
    dwn: "../../assets/RPG_C++_Console.zip",
    thumbnail: "../../assets/img/cplusplusLogo.png"
  }];  


  }

}

interface Projects{
  title:string;
  disc:string;
  type:string;
  link?:string;
  dwn?:string;
  thumbnail:string;
}

interface Option{
  name:string;
  value:string;
}